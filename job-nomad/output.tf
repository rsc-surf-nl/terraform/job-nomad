output "id" {
  value = nomad_job.job.id
}

output "jobspec" {
  value = nomad_job.job.jobspec
}

output "job_name" {
  value = nomad_job.job.name
}

output "subscription" {
  value = var.subscription
}

output "application_type" {
  value = var.application_type
}

output "resource_type" {
  value = var.resource_type
}

output "cloud_type" {
  value = var.cloud_type
}

output "subscription_group" {
  value = var.subscription_group
}

output "description" {
  value = var.description
}
