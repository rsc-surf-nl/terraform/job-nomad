terraform {
  required_providers {
    nomad = {
      version = "1.4.16"
    }
    http = {
      version = "2.1.0"
    }
  }
  required_version = ">= 1.0.0"
}
