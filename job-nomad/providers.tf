provider "nomad" {
  address   = var.nomad_host
  region    = var.nomad_region
  secret_id = var.nomad_token
}
