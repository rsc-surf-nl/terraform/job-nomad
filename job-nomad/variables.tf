variable "subscription" {}
variable "workspace_id" {}
variable "application_type" {}
variable "resource_type" {}
variable "cloud_type" {}
variable "subscription_group" {}
variable "description" {}
variable "nomad_host" {
  type        = string
  description = "Nomad host address, including protocol and port"
}
variable "nomad_region" {
  type        = string
  description = "Nomad region"
}
variable "nomad_token" {
  type        = string
  description = "A Nomad ACL token"
}
variable "nomad_datacenters" {
  type        = string
  description = "Quote-escaped list of datacenters to deploy the job to."
}
variable "jobspec_file" {
  type        = string
  description = "Path to file containing jobspec downloaded by deployer"
}
