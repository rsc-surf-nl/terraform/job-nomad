resource "nomad_job" "job" {
  hcl2 {
    enabled = true
    //    vars = {
    //      "datacenters" = var.nomad_datacenters,
    //    }
  }

  jobspec = templatefile(var.jobspec_file, {
    datacenters = var.nomad_datacenters
  })


  //    vars = {
  //      "datacenters" = var.nomad_datacenters
  //
  //    }
}
